﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SuperSite
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            //*Security* Ignore requests for system type files *Security* 
            //  routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            //  routes.IgnoreRoute("{resource}.cs/{*pathInfo}");
            // routes.IgnoreRoute("{resource}.cshtml/{*pathInfo}");
            // routes.IgnoreRoute("{resource}.config/{*pathInfo}");
            // routes.IgnoreRoute("{resource}.asax/{*pathInfo}");


            routes.MapRoute(
            name: "empty",
            url: "",
            defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
            name: "Home",
            url: "Home",
            defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
            name: "Default",
            url: "{controller}/{action}/{id}",
            defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
                );
            

        }
    }
}
