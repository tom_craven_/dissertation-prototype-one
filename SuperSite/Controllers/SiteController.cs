﻿using SuperSite.Context;
using SuperSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace SuperSite.Controllers
{
    public class SiteController : Controller
    {
        SiteContext db = new SiteContext();

        // GET: Site
        public ActionResult Index()
        {
            return View(db.siteTheme.ToList());
        }

        // GET: Site/Details/5
        public ActionResult Details(int? id)
        {
            if (id==null)
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Site site = db.siteTheme.Find(id);
            if (site == null)
            {
                return HttpNotFound();
            }return View(site);
               
        }

        // GET: Site/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Site/Create
        [HttpPost]
        public ActionResult Create(Site siteTheme)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.siteTheme.Add(siteTheme);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(siteTheme);
            }
            catch
            {
                return View();
            }
        }

        // GET: Site/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Site siteTheme = db.siteTheme.Find(id);
            if (siteTheme == null)
            {
                return HttpNotFound();
            }
            return View(siteTheme);
        }

        // POST: Site/Edit/5
        [HttpPost]
        public ActionResult Edit(Site siteTheme)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(siteTheme).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(siteTheme);
            }
            catch
            {
                return View();
            }
        }

        // GET: Site/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Site siteTheme = db.siteTheme.Find(id);
            if (siteTheme == null)
            {
                return HttpNotFound();
            }
            return View(siteTheme);
        }

        // POST: Site/Delete/5
        [HttpPost]
        public ActionResult Delete(int? id, Site siteThemee)
        {
            try
            {
                Site siteTheme = new Site();
                if (ModelState.IsValid)
                {
                    if (id == null)
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                    siteTheme = db.siteTheme.Find(id);
                    if (siteTheme == null)
                    {
                        return HttpNotFound();
                    }
                    db.siteTheme.Remove(siteTheme);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(siteTheme);
            }
            catch
            {
                return View();
            }
        }
    }
}
