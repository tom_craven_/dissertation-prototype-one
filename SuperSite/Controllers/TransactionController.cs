﻿using SuperSite.Context;
using SuperSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace SuperSite.Controllers
{
    public class TransactionController : Controller
    {
        TransactionContext db = new TransactionContext();
        // GET: Transaction
        public ActionResult Index()
        {
            return View(db.transactions.ToList());
        }

        // GET: Transaction/Details/5
        public ActionResult Details(int? id)
        {
            {
                if (id == null)
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                Transaction transaction = db.transactions.Find(id);
                if (transaction == null)
                {
                    return HttpNotFound();
                }
                return View(transaction);
            }
        }

        // GET: Transaction/Create
        public ActionResult Create()
        {
            ViewData["Stocks"] = db.stocks.ToList();
            return View();
        }

        // POST: Transaction/Create
        [HttpPost]
        public ActionResult Create(Transaction transaction)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.transactions.Add(transaction);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(transaction);
            }
            catch
            {
                return View();
            }
        }

        // GET: Transaction/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Transaction transaction = db.transactions.Find(id);
            if (transaction == null)
            {
                return HttpNotFound();
            }
            return View(transaction);
        }

        // POST: Transaction/Edit/5
        [HttpPost]
        public ActionResult Edit(Transaction transaction)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(transaction).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(transaction);
            }
            catch
            {
                return View();
            }
        }

        // GET: Transaction/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Transaction transaction = db.transactions.Find(id);
            if (transaction == null)
            {
                return HttpNotFound();
            }
            return View(transaction);
        }

        // POST: Transaction/Delete/5
        [HttpPost]
        public ActionResult Delete(int? id, Transaction transactionn)
        {
            try
            {
                Transaction transaction = new Transaction();
                if (ModelState.IsValid)
                {
                    if (id == null)
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                    transaction = db.transactions.Find(id);
                    if (transaction == null)
                    {
                        return HttpNotFound();
                    }
                    db.transactions.Remove(transaction);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(transaction);
            }
            catch
            {
                return View();
            }
        }
    }
}
