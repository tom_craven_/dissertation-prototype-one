﻿using SuperSite.Context;
using SuperSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace SuperSite.Controllers
{
    public class StockController : Controller
    {
        StockContext db = new StockContext();
        // GET: Stock
        public ActionResult Index()
        {
          
            return View(db.stocks.ToList());
        }

        // GET: Stock/Details/5
        public ActionResult Details(int? id)
        {
            {
                if (id == null)
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                Stock stock = db.stocks.Find(id);
                if (stock == null)
                {
                    return HttpNotFound();
                }
                return View(stock);
            }
        }

        // GET: Stock/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Stock/Create
        [HttpPost]
        public ActionResult Create(Stock stock)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.stocks.Add(stock);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(stock);
            }
            catch
            {
                return View();
            }
        }

        // GET: Stock/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Stock stock = db.stocks.Find(id);
            if (stock == null)
            {
                return HttpNotFound();
            }
            return View(stock);
        }

        // POST: Stock/Edit/5
        [HttpPost]
        public ActionResult Edit(Stock stock)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(stock).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(stock);
            }
            catch
            {
                return View();
            }
        }

        // GET: Stock/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Stock stock = db.stocks.Find(id);
            if (stock == null)
            {
                return HttpNotFound();
            }
            return View(stock);
        }

        // POST: Stock/Delete/5
        [HttpPost]
        public ActionResult Delete(int? id, Stock stockk)
        {
            try
            {
                Stock stock = new Stock();
                if (ModelState.IsValid)
                {
                    if (id == null)
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                    stock = db.stocks.Find(id);
                    if (stock == null)
                    {
                        return HttpNotFound();
                    }
                    db.stocks.Remove(stock);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(stock);
            }
            catch
            {
                return View();
            }
        }
    }
}
