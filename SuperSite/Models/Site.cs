﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SuperSite.Models
{
    public class Site
    {
        [Key]
        public int SiteId { set; get; }
        public string title { set; get; }
        public string backgroundColor { set; get; }

    }
}