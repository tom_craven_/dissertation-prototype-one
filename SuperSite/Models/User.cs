﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SuperSite.Models
{

    public class User
    {
        [Key]
        public int UserId { set; get; }
        public string name { set; get; }
        public bool? IsAdmin { get; set; }
    }
}