﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SuperSite.Models
{
    public class Transaction
    {   [Key]
        public int TransactionId { set; get; }
        public int quantity { set; get; }
        //#Todo : acccess control the creation of this object

        public int StockId { get; set; }
        public virtual Stock stock { get; set; }

    }
}