﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SuperSite.Models
{
    public class Stock
    {   [Key]
        public int StockId { set; get; }
        public string name { set; get; }
        public double price { set; get; }

        public virtual List<Transaction> transactions { get; set; }
        
    }
}