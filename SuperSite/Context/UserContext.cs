﻿using SuperSite.Models;

using System.Data.Entity;


namespace SuperSite.Context
{
    [DbConfigurationType(typeof(MySql.Data.Entity.MySqlEFConfiguration))]
    public class UserContext : DbContext
    {
        public DbSet<User> users { get; set; }
    }
}