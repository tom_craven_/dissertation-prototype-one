﻿using SuperSite.Models;

using System.Data.Entity;


namespace SuperSite.Context
{
    [DbConfigurationType(typeof(MySql.Data.Entity.MySqlEFConfiguration))]
    public class SiteContext : DbContext
    {
        public DbSet<Site> siteTheme { get; set; }
    }
}