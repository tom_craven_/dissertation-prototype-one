﻿using SuperSite.Models;

using System.Data.Entity;


namespace SuperSite.Context
{
    [DbConfigurationType(typeof(MySql.Data.Entity.MySqlEFConfiguration))]
    public class StockContext : DbContext
    {
      public DbSet<Stock> stocks { get; set; }
    }
}