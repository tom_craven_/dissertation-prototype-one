﻿using SuperSite.Models;

using System.Data.Entity;


namespace SuperSite.Context
{
   [DbConfigurationType(typeof(MySql.Data.Entity.MySqlEFConfiguration))]
    public class TransactionContext : DbContext
    {
        public DbSet<Transaction> transactions
        { get; set; }

        public DbSet<Stock> stocks { get; set; }
    }
}